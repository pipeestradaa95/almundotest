const fs = require('fs');
const path = require('path');
const NODE_ENV = process.env.NODE_ENV;
let configBuffer = null;

switch (NODE_ENV) {
  case 'production':
    configBuffer = fs.readFileSync(path.resolve(__dirname, 'production.json'), 'utf-8');
    break;
  case 'develop':
    configBuffer = fs.readFileSync(path.resolve(__dirname, 'develop.json'), 'utf-8');
    break;
  default:
    configBuffer = fs.readFileSync(path.resolve(__dirname, 'local.json'), 'utf-8');
}

let config = JSON.parse(configBuffer);
module.exports = config;
