// imports

var express = require("express"),
app = express(),
bodyParser  = require("body-parser"),
http     = require("http"),
server   = http.createServer(app),
methodOverride = require("method-override"),
mongoose = require('mongoose');

//global variables
global.__base = __dirname + '/';

//configurations
const config = require(__base + 'config');

// port
var port = config.server.port;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

//API PERSISTENCE
var hotelsPersistence = require('./src/app/hotels/persistence/hotelSchema')(app, mongoose);

//API CONTROLLERS
var HotelsCtrl = require('./src/app/hotels/hotelsController');

//API ROUTES
var hotels = express.Router();

hotels.route('/hotels')
  .get(HotelsCtrl.findAllHotels)
  .post(HotelsCtrl.addHotels);


hotels.route('/hotel')
  .post(HotelsCtrl.addHotel)
  .put(HotelsCtrl.findByFilters);

hotels.route('/hotel/:id')
  .get(HotelsCtrl.findById)
  .put(HotelsCtrl.updateHotel)
  .delete(HotelsCtrl.deleteHotel);

app.use('/api', hotels);


// Connection to DB
mongoose.connect('mongodb://localhost/almundoTest', function(err, res) {
  if(err) {
    throw err;
  }
  console.log('Connected to Database');
});

app.listen(port, function() {
  console.log("Node server running on port " + port);
});
