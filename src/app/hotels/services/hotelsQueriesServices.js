"use strict";

let mongoose = require('mongoose');
let Hotel  = mongoose.model('Hotels');

this.findAllHotels = function(req, res){
  Hotel.find(function(err, hotels) {
    console.log('GET /hotels')
    if(err) {
      return res.status(500).jsonp(err.message);
    }
    return res.status(200).jsonp(hotels);
  });
}

this.findById = function(req, res) {
	Hotel.findOne({"id" : req.params.id}, function(err, hotel) {
    if(err) return res.status(500).send(err.message);

    console.log('GET /hotel/' + req.params.id);
		res.status(200).jsonp(hotel);
	});
};

this.findByFilters = function(req, res){
	console.log('PUT /hotel');
	let name = req.body.name;
	if(name === undefined){
		name = "";
	}

	let starsArray = req.body.stars;
	if(starsArray === undefined){
		starsArray = [1,2,3,4,5];
	}

	Hotel.find({
		name: {$regex : name, $options: 'i'},
		stars: { $in: starsArray }
	}, function(err, hotels){
		res.status(200).jsonp(hotels);
	});

};
