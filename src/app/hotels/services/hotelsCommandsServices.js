var mongoose = require('mongoose');
var Hotel  = mongoose.model('Hotels');

this.addHotel = function(req, res) {
	console.log('POST /hotel');

	var hotel = new Hotel({
		id:           req.body.id,
		name: 	      req.body.name,
		stars:        req.body.stars,
		price:        req.body.price,
		image:        req.body.image,
		amenities:    req.body.amenities
	});

	hotel.save(function(err, hotel) {
		if(err) return res.status(500).send( err.message);
    res.status(200).jsonp(hotel);
	});
};

this.addHotels = function(req, res) {
  console.log('POST /hotels');
  req.body.forEach(function(element) {
    var hotel = new Hotel({
      id:           element.id,
      name:         element.name,
      stars:        element.stars,
      price:        element.price,
      image:        element.image,
      amenities:    element.amenities
    });

    hotel.save(function(err, hotel) {
      if(err) return res.status(500).send( err.message);
    });
  });
  res.status(200).jsonp("Registros ingresados con exito");
};

this.updateHotel = function(req, res) {
  console.log('PUT /hotel/' + req.params.id);
  let query = {"id" : req.params.id};
  let update = {
		id:           req.body.id,
		name: 	      req.body.name,
		stars:        req.body.stars,
		price:        req.body.price,
		image:        req.body.image,
		amenities:    req.body.amenities
	}
	Hotel.findOneAndUpdate(query,{$set : update}, function(err, hotel) {
    if(err) return res.send(500, err.message);
    res.status(200).jsonp(hotel);
	});
};

this.deleteHotel = function(req, res) {
  console.log('DELETE /hotel/' + req.params.id);

  let query = {"id" : req.params.id};
	Hotel.findOneAndRemove(query, function(err, hotel) {
    if(err) return res.send(500, err.message);
    res.status(200).jsonp("hotel eliminado con exito");
	});
};
