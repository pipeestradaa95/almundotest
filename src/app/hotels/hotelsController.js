const hotelsQueriesServices = require('./services/hotelsQueriesServices');
const hotelsCommandsServices = require('./services/hotelsCommandsServices');

//GET - Return all hotels in the DB
exports.findAllHotels = function(req, res) {
	hotelsQueriesServices.findAllHotels(req, res);
};

//GET - Return a hotel with specified ID
exports.findById = function(req, res) {
	hotelsQueriesServices.findById(req,res);
};

//POST - Insert a new hotel in the DB
exports.addHotel = function(req, res) {
	hotelsCommandsServices.addHotel(req,res);
};

//POST - Insert a list of new hotel in the DB
exports.addHotels = function(req, res) {
	hotelsCommandsServices.addHotels(req,res);
};

//PUT - Update a hotel that already exists
exports.updateHotel = function(req, res) {
	hotelsCommandsServices.updateHotel(req,res);
};

//DELETE - Delete a hotel that already exists
exports.deleteHotel = function(req, res) {
	hotelsCommandsServices.deleteHotel(req,res);
};

//GET - Get Hotels by filters
exports.findByFilters = function(req, res){
	hotelsQueriesServices.findByFilters(req,res);
};
