exports = module.exports = function(app, mongoose) {
  var mongoose = require('mongoose'),
      Schema   = mongoose.Schema;

  var hotelSchema = new Schema({
    id:       { type: String },
    name:     { type: String },
    stars:    { type: Number },
    price:    { type: Number },
    image:    { type: String },
    amenities:{ type: Array }
  });

  module.exports = mongoose.model('Hotels', hotelSchema);
}
